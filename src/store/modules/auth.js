import Vue from 'vue'
// import JwtService from "@/utils/jwt.service.js"

import { MessageBox, Message } from 'element-ui'
import {
  LOGIN,
  LOGOUT,
  REGISTER,
  CHECK_AUTH,
  UPDATE_USER
} from "@/store/actions.type"
import { SET_AUTH, SET_ERROR, REMOVE_AUTH, SET_NOTIFICATION, SET_NAME, SET_AVATAR, SET_TOKEN } from "@/store/mutations.type"
import { getToken, setToken, removeToken } from '@/utils/auth'
import { ApiService, AuthService } from "@/api/service.js"

const state = {
  errors: null,
  user: {},
  // isAuthenticated: !!JwtService.getToken()

  token: getToken(),
  name: '',
  avatar: ''
}

const getters = {
  currentUser(state) {
    return state.user
  },
  // name(state) {
  //   return state.name
  // },
  // avatar(state) {
  //   return state.avatar
  // }
  // isAuthenticated(state) {
  //   return state.isAuthenticated
  // }
}

const actions = {
  async[LOGIN](context, userInfo) {
    try {
      const { data } = await AuthService.login(userInfo)
      debugger
      if (data.success == false) {
        context.commit(`app/${SET_NOTIFICATION}`, { type: 'error', title: 'FAIL', message: data.msg })
      } else {
        // context.commit(SET_TOKEN, data.token)
        setToken(data.token)
        context.commit(SET_NAME, data.data.full_name)
        context.commit(SET_AVATAR, "google.com")
        return data
      }

    } catch (error) {
      context.commit(SET_NOTIFICATION, { type: 'error', title: 'FAIL', message: 'Login failed' })
    }

  },
  async [LOGOUT](context) {
    try {
      // const { data } = await AuthService.logout()
      const data = {
        success: true,
        token: getToken()
      }
      if (data.success == false) {
        context.commit(SET_NOTIFICATION, { type: 'error', title: 'FAIL', message: data.msg })
      } else {
        removeToken(data.token)
        context.commit(SET_NAME, "")
        context.commit(SET_AVATAR, "")
        return data
      }
    } catch (error) {
      context.commit(SET_NOTIFICATION, { type: 'error', title: 'FAIL', message: error.message })
    }
  },
  // [REGISTER](context, userInfo) {
  //   return new Promise((resolve, reject) => {
  //     ApiService.post("users/create", { user: userInfo })
  //       .then(({ data }) => {
  //         context.commit(SET_AUTH, data.user)
  //         resolve(data)
  //       })
  //       .catch(({ response }) => {
  //         context.commit(SET_ERROR, response.data.errors)
  //         reject(response)
  //       })
  //   })
  // },
  // [CHECK_AUTH](context) {
  //   if (JwtService.getToken()) {
  //     ApiService.setHeader()
  //     ApiService.get("user")
  //       .then(({ data }) => {
  //         context.commit(SET_AUTH, data.user)
  //       })
  //       .catch(({ response }) => {
  //         context.commit(SET_ERROR, response.data.errors)
  //       })
  //   } else {
  //     context.commit(REMOVE_AUTH)
  //   }
  // }
}

const mutations = {
  [SET_TOKEN]: (state, token) => {
    state.token = token
  },
  [SET_NAME]: (state, name) => {
    state.name = name
  },
  [SET_AVATAR]: (state, avatar) => {
    state.avatar = avatar
  },
  [REMOVE_AUTH](state) {

  }
}

export default {
  state,
  actions,
  mutations,
  getters
}
