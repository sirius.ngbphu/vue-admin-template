import Cookies from 'js-cookie'
import { Notification } from 'element-ui';
import { SET_NOTIFICATION } from '@/store/mutations.type'

const state = {
  sidebar: {
    opened: Cookies.get('sidebarStatus') ? !!+Cookies.get('sidebarStatus') : true,
    withoutAnimation: false
  },
  device: 'desktop',
  loadingScreen: false
}

const mutations = {
  TOGGLE_SIDEBAR: state => {
    state.sidebar.opened = !state.sidebar.opened
    console.log(state.sidebar.opened)
    state.sidebar.withoutAnimation = false
    if (state.sidebar.opened) {
      Cookies.set('sidebarStatus', 1)
    } else {
      Cookies.set('sidebarStatus', 0)
    }
  },
  CLOSE_SIDEBAR: (state, withoutAnimation) => {
    Cookies.set('sidebarStatus', 0)
    state.sidebar.opened = false
    state.sidebar.withoutAnimation = withoutAnimation
  },
  TOGGLE_DEVICE: (state, device) => {
    state.device = device
  },
  [SET_NOTIFICATION]: (state, noti) => {
    switch (noti.type) {
      case 'success':
        Notification.success({ title: noti.title, message: noti.message, type: noti.type })
        break
      case 'warning':
        Notification.warning({ title: noti.title, message: noti.message, type: noti.type })
        break
      case 'error':
        Notification.error({ title: noti.title, message: noti.message, type: noti.type })
        break
      case 'info':
        Notification.info({ title: noti.title, message: noti.message, type: noti.type })
        break
      default:
        Notification.info({ title: noti.title, message: noti.message, type: noti.type })
        break
    }
  },
}

const actions = {
  toggleSideBar({ commit }) {
    commit('TOGGLE_SIDEBAR')
  },
  closeSideBar({ commit }, { withoutAnimation }) {
    commit('CLOSE_SIDEBAR', withoutAnimation)
  },
  toggleDevice({ commit }, device) {
    commit('TOGGLE_DEVICE', device)
  },
  [SET_NOTIFICATION](context, noti) {
    Notification.success({ title: noti.title, message: noti.message, type: noti.type })
  }
}

const getters = {
  loadingScreen: state => {
    return state.loadingScreen
  }
}
export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
// export const appUtil = {
//   mutations: 
// }