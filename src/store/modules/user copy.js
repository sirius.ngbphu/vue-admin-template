import { login, logout, getInfo } from '@/api/user'
import { getToken, setToken, removeToken } from '@/utils/auth'
import { resetRouter } from '@/router'
import { AuthService } from '@/api/service'

const state = {
  token: getToken(),
  name: '',
  avatar: ''
}

const mutations = {
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_NAME: (state, name) => {
    state.name = name
  },
  SET_AVATAR: (state, avatar) => {
    state.avatar = avatar
  }
}

const actions = {
  // user login
  login({ commit }, userInfo) {
    debugger
    const { username, password } = userInfo
    return new Promise((resolve, reject) => {
      login({ user_login: username.trim(), password: password }).then(response => {
        debugger
        const { data } = response
        commit('SET_TOKEN', data.token)
        setToken(data.token)
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },
  async['LoginApp'](context, userInfo) {
    debugger
    let res = null
    try {
      res = await AuthServiceService.login(userInfo)
      // context.commit(SET_CATEGORY, res.data.data)
      context.commit(SET_NOTIFICATION, { type: 'success', title: 'SUCCESS', message: 'Login successfully' })
      context('SET_TOKEN', data.token)
      setToken(data.token)
      router.replace({ path: '/category/' + res.data.data })
    } catch (error) {
      context.commit(SET_NOTIFICATION, { type: 'danger', title: 'FAIL', message: 'Add catergory failed' })
    } finally {
      return res
    }
  },

  // get user info
  getInfo({ commit, state }) {
    return new Promise((resolve, reject) => {
      getInfo(state.token).then(response => {
        const { data } = response

        if (!data) {
          reject('Verification failed, please Login again.')
        }

        const { name, avatar } = data

        commit('SET_NAME', name)
        commit('SET_AVATAR', avatar)
        resolve(data)
      }).catch(error => {
        reject(error)
      })
    })
  },

  // user logout
  logout({ commit, state }) {
    return new Promise((resolve, reject) => {
      logout(state.token).then(() => {
        commit('SET_TOKEN', '')
        removeToken()
        resetRouter()
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  // remove token
  resetToken({ commit }) {
    return new Promise(resolve => {
      commit('SET_TOKEN', '')
      removeToken()
      resolve()
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

