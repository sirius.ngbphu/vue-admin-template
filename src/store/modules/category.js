import Vue from 'vue'
import router from '@/router'
import { Notification } from 'element-ui'

import { CategoryService, AuthService } from '@/api/service'
import JwtService from '@/utils/jwt.service'

import {
  CATEGORY_INSERT, CATEGORY_UPDATE, CATEGORY_DELETE, CATEGORY_LIST_PAGE, CATEGORY_FETCH, CATEGORY_RESET_STATE
} from '@/store/actions.type'
import { SET_CATEGORIES, SET_CATEGORY, SET_ERROR, RESET_STATE, SET_NOTIFICATION } from '@/store/mutations.type'


// const dùng để khởi tạo 1 đối tượng category
const initialState = {
  category: {
    name: '',
    code: '',
    description: ''
  },
  categories: [],
  errors: null,
}

export const state = { ...initialState }
const actions = {
  async[CATEGORY_INSERT](context, category) {
    try {
      let res = await CategoryService.insert(category)
      if (res.data.result === 1) {
        context.commit(SET_NOTIFICATION, { type: 'success', title: 'SUCCESS', message: 'Add catergory successfully' })
        router.replace({ path: '/category/' + res.data.data })
      } else {
        context.commit(SET_NOTIFICATION, { type: 'error', title: res.data.message, message: res.data.error_msg })
      }
    } catch (error) {
      context.commit(SET_NOTIFICATION, { type: 'error', title: 'FAIL', message: error.message })
    }
  },
  async[CATEGORY_UPDATE](context, category) {
    try {
      let res = await CategoryService.update(category)
      debugger
      if (res.data.result === 1) {
        context.commit(SET_CATEGORY, res.data.data)
        context.commit(`app/${SET_NOTIFICATION}`, { type: 'success', title: 'SUCCESS', message: 'Update catergory successfully' })
      } else {
        context.commit(`app/${SET_NOTIFICATION}`, { type: 'error', title: res.data.message, message: res.data.error_msg })
      }
    } catch (error) {
      context.commit(`app/${SET_NOTIFICATION}`, { type: 'error', title: 'FAIL', message: error.message })
    }
  },
  async[CATEGORY_DELETE](context, category) {
    try {
      const { res } = await CategoryService.delete(category)
      if (res.data.result === 1) {
        context.commit(`app/${SET_NOTIFICATION}`, { type: 'success', title: 'SUCCESS', message: 'Delete catergory successfully' })
        context.dispatch(CATEGORY_LIST_PAGE)
      } else {
        context.commit(`app/${SET_NOTIFICATION}`, { type: 'error', title: res.data.message, message: res.data.error_msg })
      }
    } catch (error) {
      context.commit(`app/${SET_NOTIFICATION}`, { type: 'error', title: 'FAIL', message: error.message })
    }
  },
  async[CATEGORY_FETCH](context, id) {
    try {
      let res = await CategoryService.get(id)
      context.commit(SET_CATEGORY, res.data.data)
    } catch (error) {
      context.commit(`app/${SET_NOTIFICATION}`, { type: 'error', title: 'FAIL', message: error.message })
    }
  },
  async[CATEGORY_LIST_PAGE](context, params) {
    try {
      context.commit("app/startLoading", true)
      let res = await CategoryService.queryByPage(params)
      context.commit(SET_CATEGORIES, res.data.data.results)
      context.commit("app/startLoading", false)
      return res.data.data.total
    } catch (error) {
      console.log(error)
      context.commit(`app/${SET_NOTIFICATION}`, { type: 'error', title: 'FAIL', message: error.message })
    }
  },
  [CATEGORY_RESET_STATE]({ commit }) {
    commit(RESET_STATE)
  }
}

const mutations = {
  [SET_ERROR](state, error) {
    state.errors = error
  },
  [SET_CATEGORY](state, category) {
    state.category = category
  },
  [SET_CATEGORIES](state, categories) {
    state.categories = categories
  },
  [RESET_STATE]() {
    for (let f in state) {
      Vue.set(state, f, initialState[f])
    }
  },
}
const getters = {
  // currentUser(state) {
  //   return state.user
  // },
  // isAuthenticated(state) {
  //   return state.isAuthenticated
  // },
  categories: state => {
    return state.categories
  },
  category: state => {
    return state.category
  }
}

export default {
  state,
  actions,
  mutations,
  getters
}