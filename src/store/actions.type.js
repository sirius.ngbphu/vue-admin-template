
export const TEST = "test"
export const LOGIN = "login"
export const LOGOUT = "logout"
export const CHECK_AUTH = "checkAuth"
export const REGISTER = "register"
export const UPDATE_USER = "updateUser"

export const CATEGORY_INSERT = "insertCategory"
export const CATEGORY_UPDATE = "updateCategory"
export const CATEGORY_DELETE = "deleteCategory"
export const CATEGORY_LIST_PAGE = "lstPageCategory"
export const CATEGORY_FETCH = "fetchCategoryById"
export const CATEGORY_RESET_STATE = "resetStateCategory"


