import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";
import { API_URL, DEV_URL } from "@/utils/config";
import router from '@/router'
import { getToken, setToken, removeToken } from '@/utils/auth'

const ApiService = {
  init() {
    Vue.use(VueAxios, axios);
    Vue.axios.defaults.baseURL = DEV_URL;
    Vue.axios.interceptors.request.eject(resInterceptor);
  },

  setHeader() {
    Vue.axios.defaults.headers.common[
      "Authorization"
    ] = getToken()
  },

  query(resource, params, checkAuth = true) {
    if (checkAuth)
      this.setHeader()
    return Vue.axios.get(resource, params).catch(error => {
      throw new Error(`ApiService ${error}`);
    });
  },

  get(resource, id = "", checkAuth = true) {
    if (checkAuth)
      this.setHeader()
    return Vue.axios.get(`${resource}/${id}`).catch(error => {
      throw new Error(`ApiService ${error}`);
    });
  },

  post(resource, params, checkAuth = true) {
    if (checkAuth)
      this.setHeader()
    return Vue.axios.post(`${resource}`, params).catch(error => {
      throw new Error(`ApiService ${error}`);
    });
  },

  update(resource, id, params, checkAuth = true) {
    if (checkAuth)
      this.setHeader()
    return Vue.axios.put(`${resource}/${id}`, params);
  },

  put(resource, params, checkAuth = true) {
    if (checkAuth)
      this.setHeader()
    return Vue.axios.put(`${resource}`, params);
  },

  delete(resource, checkAuth = true) {
    if (checkAuth)
      this.setHeader()
    return Vue.axios.delete(resource).catch(error => {
      throw new Error(`ApiService ${error}`);
    });
  }
};
// check response is auth?
export const resInterceptor = axios.interceptors.response.use(
  response => {
    const res = response.data
    if (res.err_code == 401) {
      removeToken()
      router.replace({ path: '/login' })
      return Promise.reject(new Error(res.message || 'Error'))
    } else {
      return response
    }
  },
  error => {
    console.log('err' + error) 
    return Promise.reject(error)
  }
)


export default ApiService;

export const AuthService = {
  test() {
    return ApiService.get("test", false); //false: without token in header
  },
  login(params) {
    return ApiService.post("login", params, false);
  },
  logout() {
    return ApiService.post("logout");
  }
};

export const CategoryService = {
  queryByPage(params) {
    return ApiService.post("category/get-by-page", params)
  },
  queryAll(params) {
    return ApiService.query("category/get-all", {
      params: params
    });
  },
  get(id) {
    return ApiService.get("category/detail?id=" + id)
  },
  insert(params) {
    return ApiService.post("category/insert", params)
  },
  update(params) {
    return ApiService.put("category/update", params)
  },
  delete(params) {
    return ApiService.post('category/delete', params)
  }
};