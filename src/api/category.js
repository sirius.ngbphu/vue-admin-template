import { ApiService } from '@/api/service'
export const CategoryService = {
    queryByPage(params) {
        return ApiService.post("category/get-by-page", params)
    },
    queryAll(params) {
        return ApiService.query("category/get-all", {
            params: params
        });
    },
    get(id) {
        return ApiService.get("category/detail?id=" + id)
    },
    insert(params) {
        return ApiService.post("category/insert", params)
    },
    update(params) {
        return ApiService.put("category/update", params)
    },
    delete(params) {
        return ApiService.post('category/delete', params)
    }
};